﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MensaPlanLib.Meals
{
    class HannoverMeal : Meal
    {
        protected static readonly Regex mealRegEx = new Regex(@"^> (?<cat>.+)( \((.+)\))?: (?<name>.+) (?<add>(\(\w\w?\))+) (?<pstud>\d\d?,\d\d)€/(?<pstaff>\d\d?,\d\d)€/(?<pguest>\d\d?,\d\d)€$", RegexOptions.ExplicitCapture);
        protected static readonly CultureInfo cultureDe = new CultureInfo("de-DE");

        protected string mealAsText;

        public HannoverMeal(string text, MensaPlan plan)
            : base(plan)
        {
            mealAsText = text;
        }

        internal override void parseData()
        {
            //Example: "> Wahlmenü (Komponente mit Fleisch): Marinierte Hähnchenbrust, Honig-Chili-Dip (g)(n)(k)(26)(29) 1,80€/2,70€/3,10€"

            //We use a RegEx to extract the data:
            var match = mealRegEx.Match(mealAsText);
            if (!match.Success)
            {
                Name = "Parse Error";
                Category = "Error";
                return;
            }

            Category = match.Groups["cat"].Value;
            Name = match.Groups["name"].Value;
            parseAdditives(match.Groups["add"].Value);
            PriceStudents = decimal.Parse(match.Groups["pstud"].Value, cultureDe); //force german number style (for english etc. maschines)
            PriceStaff = decimal.Parse(match.Groups["pstaff"].Value, cultureDe);
            PriceGuests = decimal.Parse(match.Groups["pguest"].Value, cultureDe);
        }

        protected virtual void parseAdditives(string add)
        {
            //we assume that plan.additives has been populated
            add = add.Replace("(", ""); //remove all opening braces, then split at the closing ones
            foreach (var a in add.Split(')'))
            {
                if (!Plan.Additives.Any(x => x.Identifier == a))
                    continue; //We found an identified, but have no addidive for this. TODO: Issue a warning here
                additives.Add(Plan.Additives.First(x => x.Identifier == a));
            }
        }
    }
}
