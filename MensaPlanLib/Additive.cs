﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MensaPlanLib
{
    public abstract class Additive
    {
        /// <summary>
        /// Gets the mensa plan this addive is assoiated with.
        /// </summary>
        public MensaPlan Plan { get; private set; }

        /// <summary>
        /// Gets the Name of this Additive.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Gets the english Name (if specified) of the Additive.
        /// </summary>
        public string EnglishName { get; protected set; }

        /// <summary>
        /// Gets the Identifier of the Addivtive. The
        /// Identifier is usually a one or two letter code.
        /// </summary>
        public string Identifier { get; protected set; }

        public Additive(MensaPlan plan)
        {
            this.Plan = plan;
        }

        internal abstract void parseData();

        public override string ToString()
        {
            return string.Format("{0}: {1}/{2}", Identifier, Name, EnglishName);
        }
    }
}
