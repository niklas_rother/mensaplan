using MensaPlanLib.MensaPlans;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace MensaPlanLib
{
    /// <summary>
    /// This class represents a mensa plan. Use the
    /// static method CreateForMensa() to create an
    /// instance of this class.
    /// </summary>
    public abstract class MensaPlan
    {
        /// <summary>
        /// Gets the mensa's name.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Get the mensa plan's date.
        /// </summary>
        public DateTime Date { get; protected set; }

        protected List<Meal> meals = new List<Meal>();

        /// <summary>
        /// Gets a read-only list of the meals on
        /// this mensaplan.
        /// </summary>
        public ReadOnlyCollection<Meal> Meals
        {
            get { return meals.AsReadOnly(); }
        }

        protected List<Additive> additives = new List<Additive>();

        /// <summary>
        /// Gets a read-only list of all addives that
        /// this mensa plan (and its meals) contain.
        /// Not every addive has to be used in a meal.
        /// </summary>
        public ReadOnlyCollection<Additive> Additives
        {
            get { return additives.AsReadOnly(); }
        }

        /// <summary>
        /// Gets a values that indicated if the mensa is
        /// closed today. If true other properties like Meals
        /// and Additives are not defined.
        /// </summary>
        public bool IsClosed { get; protected set; }

        protected MensaPlan() //protected ctor!
        { }

        /// <summary>
        /// Fabric method: Creates an instance of an subclass specific for
        /// the given Mensa.
        /// </summary>
        public static MensaPlan CreateForMensa(Mensa mensa)
        {
            MensaPlan p = null;

            switch (mensa)
            {
#if DEBUG
                case Mensa.Debug:
                    p = new DebugMensaPlan();
                    break;
#endif
                case Mensa.HannoverHauptmensa:
                    p = new HannoverMensaPlan(new Uri("http://www.stwh-portal.de/mensa/index.php?wo=2&wann=1&format=txt"));
                    break;
                case Mensa.HannoverContine:
                    p = new HannoverMensaPlan(new Uri("http://www.stwh-portal.de/mensa/index.php?wo=9&wann=1&format=txt"));
                    break;
                case Mensa.HannoverMarktstand:
                    p = new HannoverMensaPlan(new Uri("http://www.stwh-portal.de/mensa/index.php?wo=3&wann=1&format=txt"));
                    break;
                default:
                    throw new Exception("Unknown mensa " + mensa);
            }

            if (p != null)
            {
                p.downloadData();
                p.parseData();
            }

            return p;
        }

        /// <summary>
        /// Tells the MensaPlan to download it's required data (if any).
        /// </summary>
        protected abstract void downloadData(); //TODO: PascalCase
        
        /// <summary>
        /// Tell the MensaPlan to parse the downloaded data.
        /// downloadData() was called before, when this method
        /// is called.
        /// </summary>
        protected abstract void parseData();

        /// <summary>
        /// Gets a List of all meals that are suitable for the user,
        /// e.g. that DO NOT contain an additive from the exclude list
        /// but DO contain all from the include only list (both from
        /// MensaSettings).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Meal> GetAllSuitableMeals()
        {
            foreach (var m in Meals)
            {
                if (m.Additives.Any(a => MensaSettingsHelper.GetMealsToHide().Contains(a.Identifier)))
                    continue;
                if (!MensaSettingsHelper.GetMealsToShowOnly().All(a => m.Additives.Any(a2 => a2.Identifier == a))) //TODO: This works but looks a bit weired
                    continue;
                yield return m;
            }
        }
    }
}

