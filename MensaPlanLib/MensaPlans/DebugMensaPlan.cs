﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MensaPlanLib.MensaPlans
{
    class DebugMensaPlan : HannoverMensaPlan
    {
        //The Debug-Mensa is a Hannover-Mensa with fixed text

        public DebugMensaPlan()
            : base(null)
        { }

        protected override void downloadData()
        {
            planAsText = DemoTextOpen;
            //planAsText = DemoTextClosed;
        }

        #region DemoText
        public const string DemoTextOpen = @"
Debug Mensa

# Montag 16.12.2013 #
> Eintopf: Geflügeleintopf (g)(n)(28) 1,35€/2,50€/3,50€
> Tellergericht: Hausgemachte Käsespätzle, Römersalat, Italien-Dressing (v)(a)(n)(20)(22)(26)(29) 1,85€/3,20€/4,00€
> Wahlmenü (Komponente mit Fleisch): Marinierte Hähnchenbrust, Honig-Chili-Dip (g)(n)(k)(26)(29) 1,80€/2,70€/3,10€
> Wahlmenü (Komponente ohne Fleisch): Blumenkohlcurry (v)(n)(28) 1,70€/2,60€/3,00€
> Wahlmenü (Beilage): Kroketten (v)(1)(20) 0,60€/0,90€/1,10€
> Wahlmenü (Beilage): Reis (v)(n) 0,60€/0,90€/1,10€
> Wahlmenü (Gemüse): Erbsengemüse (v)(n) 0,65€/0,95€/1,15€
> Wahlmenü (Gemüse): Rosenkohl (v)(n) 0,65€/0,95€/1,15€
> Wahlmenü (Salat): Eisbergsalat (v)(n) 0,60€/0,90€/1,10€
> Wahlmenü (Suppe): Tomatisierte-Bohnen-Suppe (v)(n)(26)(28)(29) 0,40€/0,50€/0,70€
> Wahlmenü (Dessert): Aprikosenjoghurt (v)(n)(26) 0,80€/0,95€/1,10€


- LEGENDE:
(1) Farbstoff / color
(2) Konservierungsstoff / preservative
(3) Antioxidationsmittel / antioxidant
(4) Geschmacksverstärker / flavour enhancer
(5) geschwefelt / sulphite
(6) geschwärzt / firming agent
(7) gewachst / glazing agent
(8) Phosphat / phosphate
(9) Süßungsmittel / sweetener
(12) mit Nitritpökelsalz
(20) Glutenhaltiges Getreide
(21) Krebstiere und Krebstiererzeugbisse
(22) Eier und Eiererzeugnisse
(23) Fisch und Fischerzeugnisse
(24) Erdnüsse und Erdnusserzeugnisse
(25) Soja und Sojaerzeugnisse
(26) Milch und Milcherzeugnisse
(27) Schalenfrüchte
(28) Sellerie und Sellerieerzeugnisse
(29) Senf und Senferzeugnisse
(30) Sesamsamen und Sesamsamenerzeugnisse
(31) Schwefeldioxid und Sulfite > 10 mg/kg
(32) Lupine und Lupinenerzeugnisse
(33) Weichtiere und Weichtiererzeugnisse
(s) Schweinefleisch / pork
(r) Rindfleisch / beef
(v) ohne Fleisch / without meat
(j) artger. Tierhaltung / species-appropr. husbandry
(a) mit Alkohol / contains alcohol
(f) nachhaltige Fischerei / sustainable fishery
(n) Natürlich frisch! / natural(ly) fresh!
(k) mit Knoblauch / contains garlic
";
        public const string DemoTextClosed = @"
Debug Mensa

# Dienstag 31.12.2013 #
Geschlossen

- LEGENDE:
(1) Farbstoff / color
(2) Konservierungsstoff / preservative
(3) Antioxidationsmittel / antioxidant
";
        #endregion
    }
}
