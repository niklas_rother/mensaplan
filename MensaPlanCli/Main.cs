using System;
using MensaPlanLib;
using Edokan.KaiZen.Colors;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace MensaPlanCli
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //TODO: This will fix the encoding problems, IF the font is set to something other than the default, otherwise it will make it worse...
            Console.OutputEncoding = Encoding.UTF8;

            if (MensaPlanLib.Properties.Settings.Default.ShowCliColor) //TOOD: This will not really disable the colors...
            {
                EscapeSequencer.Install();
                EscapeSequencer.Bold = true;
            }

            if (MensaPlanLib.Properties.Settings.Default.ShowCliLogo)
                Console.WriteLine("MensaPlan CLI".White() + " by " + "Niklas Rother\n");

            //Parse the command line //TODO: Put this somewhere more reasonable
            string verb = "show"; //Default
            if (args.Length >= 1)
                verb = args[0];

            switch (verb)
            {
                case "show":
                    HandleShowVerb(args);
                    break;
                case "config":
                    HandleConfigVerb(args);
                    break;
                case "help":
                    ShowHelp();
                    break;
                default:
                    Console.WriteLine("Error: ".Red() + verb.White() + " is not a know verb.\n");
                    goto case "help";
            }
        }

        private static void ShowHelp()
        {
            Console.WriteLine("MensaPlanCli [verb] [args...]\n");
            Console.WriteLine("Available verbs:");
            Console.WriteLine("\tshow - Show the Mensaplan. This is the default verb.\n");
            Console.WriteLine("\tconfig - Show or change the configuration");
            Console.WriteLine("\t\t-l|--list - list all the config options with the values.");
            Console.WriteLine("\t\t-a|--add key value - add the value to the config option key. Must be used with lists as key.");
            Console.WriteLine("\t\t-r|--remove key value - removes the value from the config option key. Must be used with lists as key.");
            Console.WriteLine("\t\t-c|--clear key  - clears the config option key. Must be used with lists as key.");
            Console.WriteLine("\t\t-s|--set key value - set the config option key to value. Must not be used with lists.");
            Console.WriteLine("\t\t--reset - resets the whole confiuguration to factory defaults.");
        }

        private static void HandleShowVerb(string[] args)
        {
            if (!MensaSettingsHelper.GetSelectedMensas().Any())
                Console.WriteLine("Note: ".Yellow() + "The is currently no selected mensa. Try config --reset.");

            foreach (Mensa mensa in MensaSettingsHelper.GetSelectedMensas())
            {
                MensaPlan plan = MensaPlan.CreateForMensa(mensa);

                Console.WriteLine(plan.Name.Cyan());
                Console.WriteLine(plan.Date.ToShortDateString());
                Console.WriteLine();

                if (plan.IsClosed)
                    Console.WriteLine("- Heute Geschlossen - ");
                else
                {
                    foreach (var m in plan.GetAllSuitableMeals())
                    {
                        List<string> prices = new List<string>();
                        if (MensaPlanLib.Properties.Settings.Default.ShowStudentPrice)
                            prices.Add(m.PriceStudents.ToString("c"));
                        if (MensaPlanLib.Properties.Settings.Default.ShowStaffPrice)
                            prices.Add(m.PriceStaff.ToString("c"));
                        if (MensaPlanLib.Properties.Settings.Default.ShowGuestPrice)
                            prices.Add(m.PriceGuests.ToString("c"));

                        Console.WriteLine("{0}: " + "{1}".White() + " {2}", m.Category, m.Name, string.Join("/", prices));
                    }
                }
            }
        }

        private static void HandleConfigVerb(string[] args)
        {
            string action = "";
            if (args.Length >= 2)
                action = args[1];

            switch (action)
            {
                case "-l":
                case "--list":
                    HandleConfigListAction(args);
                    break;
                case "-a":
                case "--add":
                    HandleConfigAddAction(args);
                    break;
                case "-c":
                case "--clear":
                    HandleConfigClearAction(args);
                    break;
                case "-r":
                case "--remove":
                    HandleConfigRemoveAction(args);
                    break;
                case "-s":
                case "--set":
                    HandleConfigSetAction(args);
                    break;
                case "--reset":
                    HandleConfigResetAction(args);
                    break;
                default:
                    Console.WriteLine("Error: ".Red() + action.White() + " is not a know action.\n");
                    ShowHelp();
                    break;
            }

            Console.ReadKey(false);
        }

        private static void HandleConfigResetAction(string[] args)
        {
            MensaPlanLib.Properties.Settings.Default.Reset();
            Console.WriteLine("Success: ".Green() + "The configuration was reset.");
            MensaPlanLib.Properties.Settings.Default.Save();
        }

        private static void HandleConfigSetAction(string[] args)
        {
            if (!CheckArgsForConfigAction(args))
                return;

            var key = args[2];
            var value = args[3];
            //TODO: Check if called with a list
            try
            {
                MensaPlanLib.Properties.Settings.Default[key] = Convert.ChangeType(value, MensaPlanLib.Properties.Settings.Default.Properties[key].PropertyType);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Error: ".Red() + "There was an error setting the config option. Maybe the wrong format?\n" + e.Message);
                return;
            }

            Console.WriteLine("Success: ".Green() + key.White() + " was set to " + value.White());
            MensaPlanLib.Properties.Settings.Default.Save();
        }

        private static void HandleConfigRemoveAction(string[] args)
        {
            if (!CheckArgsForListConfigAction(args))
                return;

            var list = GetConfigKeyAsStringCollection(args[2]);
            var key = args[2];
            var value = args[3];

            if (!list.Contains(value))
            {
                Console.WriteLine("Warning: ".Yellow() + value.White() + " was not in the list " + key.White());
                return;
            }

            list.Remove(value);

            Console.WriteLine("Success: ".Green() + value.White() + " removed from the config list " + key.White());
            MensaPlanLib.Properties.Settings.Default.Save();
        }

        private static void HandleConfigClearAction(string[] args)
        {
            if (!CheckArgsForListConfigAction(args))
                return;

            var list = GetConfigKeyAsStringCollection(args[2]);
            var key = args[2];

            list.Clear();
            Console.WriteLine("Success: ".Green() + key.White() + " cleared.");
            MensaPlanLib.Properties.Settings.Default.Save();
        }

        private static void HandleConfigAddAction(string[] args)
        {
            if (!CheckArgsForListConfigAction(args))
                return;

            var list = GetConfigKeyAsStringCollection(args[2]);
            var key = args[2];
            var value = args[3];

            list.Add(value);
            Console.WriteLine("Success: ".Green() + value.White() + " added to the config list " + key.White());
            MensaPlanLib.Properties.Settings.Default.Save();
        }

        private static void HandleConfigListAction(string[] args)
        {
            Console.WriteLine("Current Configuration:".White());

            foreach (System.Configuration.SettingsProperty setting in MensaPlanLib.Properties.Settings.Default.Properties)
            {
                var key = setting.Name;
                var value = MensaPlanLib.Properties.Settings.Default[key];

                if (setting.PropertyType == typeof(StringCollection))
                {
                    var collection = ((StringCollection)value).Cast<string>();
                    if (!collection.Any())
                        value = "(empty)";
                    else
                        value = string.Join(",", collection);
                }

                Console.WriteLine("\t" + key.White() + " - " + value.ToString().White());
            }
        }

        private static bool IsValidConfigKey(string key)
        {
            return MensaPlanLib.Properties.Settings.Default.Properties.Cast<System.Configuration.SettingsProperty>().Any(p => p.Name == key);
        }

        private static bool IsListConfigKey(string key)
        {
            if (!IsValidConfigKey(key))
                return false;

            var setting = MensaPlanLib.Properties.Settings.Default[key];
            if (setting == null)
                return false;
            return setting.GetType() == typeof(StringCollection);
        }

        private static StringCollection GetConfigKeyAsStringCollection(string key)
        {
            return (StringCollection)MensaPlanLib.Properties.Settings.Default[key];
        }

        private static bool CheckArgsForConfigAction(string[] args)
        {
            if (args.Length != ((new[] { "--clear", "-c" }.Contains(args[1])) ? 3 : 4)) //-c|--clear needs only the name, no value
            {
                Console.WriteLine("Error: ".Red() + "There are too many/few parameters for this action.\n");
                return false;
            }
            if (!IsValidConfigKey(args[2]))
            {
                Console.WriteLine("Error: ".Red() + args[2] + " is not a valid config key. Try " + "config --list".White());
                return false;
            }

            return true;
        }

        private static bool CheckArgsForListConfigAction(string[] args)
        {
            if (!CheckArgsForConfigAction(args))
                return false;

            if (!IsListConfigKey(args[2]))
            {
                Console.WriteLine("Error: ".Red() + args[2] + " is not a list config key. Try " + "config --list".White());
                return false;
            }

            return true;
        }
    }
}
