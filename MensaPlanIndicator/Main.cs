using System;
using System.IO;
using AppIndicator;
using Gtk;
using MensaPlanLib;
using System.Linq;

namespace MensaPlanIndicator
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Application.Init();
			
			ApplicationIndicator indicator = new ApplicationIndicator("mensaplan", "mensa-icon", Category.ApplicationStatus, Path.GetDirectoryName(Environment.GetCommandLineArgs()[0]));
			indicator.Status = Status.Active;
			
			MensaPlan plan = MensaPlan.CreateForMensa(Mensa.Debug);
			
			var menu = new Menu();
			MenuItem menuItem;
			
			menu.Append(new MenuItem(string.Format("{0} - {1}", plan.Name, plan.Date.ToShortDateString())));
			
			if (plan.IsClosed)
                menu.Append(new MenuItem("- Heute Geschlossen - "));
            else
            {
                foreach (var m in plan.Meals)
                {
                    menuItem = new MenuItem(string.Format("{0}: {1}", m.Category, m.Name));
					menuItem.Activated += (sender, e) => {
						var mbox = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok,
						                             "{0}: {1} {2:c}\n{3}", m.Category, m.Name, m.PriceStudents, m.Additives.ToArray());
						mbox.Run();
						mbox.Destroy();
					};
					menu.Append(menuItem);
                }
            }
			
			menu.Append(new SeparatorMenuItem());
			
			menuItem = new MenuItem("Über");
			menuItem.Activated += (sender, e) => {
				var mbox = new MessageDialog(null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "MensaPlan Indicator by Niklas Rother");
				mbox.Run();
				mbox.Destroy();
			};
			menu.Append(menuItem);
			
			menuItem = new MenuItem("Beenden");
			menuItem.Activated += (sender, e) => Application.Quit();
			menu.Append(menuItem);
			
			menu.ShowAll();
			
			indicator.Menu = menu;
			
			Application.Run();
		}
	}
}
