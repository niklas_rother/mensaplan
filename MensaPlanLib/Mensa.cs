using System;

namespace MensaPlanLib
{
    /// <summary>
    /// The supported Mensas.
    /// </summary>
    /// <remarks>
    /// This list has to be extend, when new sub-classes
    /// of MensaPlan are added.
    /// </remarks>
    public enum Mensa
    {
        //Demo
        /// <summary>
        /// This is a dummy Mensa, the text is hardcoded in
        /// the program.
        /// Note: Only valid, if DEBUG is defined!
        /// </summary>
        Debug,

        //Hannover Mensas
        HannoverHauptmensa,
        HannoverContine,
        HannoverMarktstand,
    }
}

