﻿using MensaPlanLib.Additives;
using MensaPlanLib.Meals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace MensaPlanLib.MensaPlans
{
    class HannoverMensaPlan : MensaPlan
    {
        protected Uri url;
        protected string planAsText;

        public HannoverMensaPlan(Uri url)
        {
            this.url = url;
        }
        
        protected override void downloadData()
        {
            planAsText = new WebClient()
                {
                    Encoding = Encoding.UTF8
                }.DownloadString(url); //TODO: Make this async?
        }

        protected override void parseData()
        {
            HannoverParserState state = HannoverParserState.Name;
            
            foreach (var line in planAsText.Split('\n'))
            {
                if (string.IsNullOrWhiteSpace(line))
                    continue;

                switch (state)
                {
                    case HannoverParserState.Name:
                        Name = line.Trim();
                        state = HannoverParserState.Date;
                        break;
                    case HannoverParserState.Date:
                        var match = Regex.Match(line, @"\d?\d\.\d?\d\.20\d\d");
                        DateTime date;
                        if (!match.Success || !DateTime.TryParse(line, out date))
                            date = DateTime.Now; //it's very likely that there would be todays date!
                        Date = date;
                        state = HannoverParserState.Meals;
                        break;
                    case HannoverParserState.Meals:
                        if (line.Contains("LEGENDE"))
                        {
                            state = HannoverParserState.Additives;
                            continue;
                        }
                        if (line.Contains("Geschlossen"))
                        {
                            IsClosed = true;
                            continue;
                        }
                        
                        Meal m = new HannoverMeal(line.Trim(), this); //Do not parse not, the addive list is empty!
                        meals.Add(m);
                        break;
                    case HannoverParserState.Additives:
                        Additive a = new HannoverAdditive(line.Trim(), this);
                        a.parseData();
                        additives.Add(a);
                        break;
                    default:
                        throw new Exception("Parse ran into unkown state " + state);
                }
            }
            foreach (var meal in meals) //Now we can do the parsing
                meal.parseData();
        }
    }
}
