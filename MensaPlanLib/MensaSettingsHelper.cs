﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MensaPlanLib
{
    /// <summary>
    ///  This class contains some helpers for the access to this
    ///  DLLs Settings. Note: Not all settings are replicated here,
    ///  access <c>MensaPlanLib.Properties.Settings.Default</c> for the rest.
    ///  You must call Save() after changing any of these settings.
    /// </summary>
    public static class MensaSettingsHelper
    {
        /// <summary>
        /// Helper method to retrive the content of the Setting
        /// <c>Mensas</c> as an IEnumerable.
        /// </summary>
        public static IEnumerable<Mensa> GetSelectedMensas()
        {
            //Uhmm, yes this is a bit complicated. Basically it splits the given string at the semicolons and return an Mensa[] with all
            //parts of the strings that could be parsed. Sorry for me loving LINQ...
            return Properties.Settings.Default.Mensas.Cast<string>().Select(x =>
            {
                Mensa ret;
                if (Enum.TryParse<Mensa>(x, true, out ret))
                    return new { Success = true, Mensa = ret };
                else
                    return new { Success = false, Mensa = ret };
            }).Where(x => x.Success).Select(x => x.Mensa);
            //TODO: This could be rewritten using the yield keyword.
        }

        /// <summary>
        /// Helper method to set the Setting <c>Mensas</c>
        /// from an IEnumerable.
        /// </summary>
        public static void SetSelectedMensas(IEnumerable<Mensa> mensas)
        {
            Properties.Settings.Default.Mensas.Clear();
            foreach (var mensa in mensas)
            {
                Properties.Settings.Default.Mensas.Add(Enum.GetName(typeof(Mensa), mensa));
            }
        }

        /// <summary>
        /// Helper method to get an IEnumlerable of the identifiers
        /// of all additives the user requested to hide.
        /// (Setting <c>HideMealsWithAdditive</c>)
        /// </summary>
        public static IEnumerable<string> GetMealsToHide()
        {
            return Properties.Settings.Default.HideMealsWithAdditives.Cast<string>();
        }

        /// <summary>
        /// Helper method to set the list of the identifiers
        /// of all additives the user requested to hide from an
        /// IEnumerable. (Setting <c>HideMealsWithAdditive</c>)
        /// </summary>
        public static void SetMealsToHide(IEnumerable<string> additives)
        {
            Properties.Settings.Default.HideMealsWithAdditives.Clear();
            foreach (var additive in additives)
            {
                Properties.Settings.Default.HideMealsWithAdditives.Add(additive);
            }

        }

        /// <summary>
        /// Helper method to get an IEnumlerable of the identifiers
        /// of all additives the user requested show only meals with.
        /// (Setting <c>ShowOnlyMealsWithAdditives</c>)
        /// </summary>
        public static IEnumerable<string> GetMealsToShowOnly()
        {
            return Properties.Settings.Default.ShowOnlyMealsWithAdditives.Cast<string>();
        }

        /// <summary>
        /// Helper method to set the list of the identifiers
        /// of all additives the user requested to show only meals with from an
        /// IEnumerable. (Setting <c>ShowOnlyMealsWithAdditives</c>)
        /// </summary>
        public static void SetMealsToShowOnly(IEnumerable<string> additives)
        {
            Properties.Settings.Default.ShowOnlyMealsWithAdditives.Clear();
            foreach (var additive in additives)
            {
                Properties.Settings.Default.ShowOnlyMealsWithAdditives.Add(additive);
            }

        }
    }
}
