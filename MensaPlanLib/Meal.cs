﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MensaPlanLib
{
    /// <summary>
    /// Represents a meal in a mensaplan.
    /// </summary>
    public abstract class Meal
    {
        /// <summary>
        /// Gets the MensaPlan this meal is associated with.
        /// </summary>
        public MensaPlan Plan { get; private set; }

        /// <summary>
        /// Gets the Name of the Meal.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Gets the Category of the Meal.
        /// </summary>
        public string Category { get; protected set; }

        /// <summary>
        /// Get the price for students of the Meal.
        /// </summary>
        public decimal PriceStudents { get; protected set; }

        /// <summary>
        /// Get the price for staff of the Meal.
        /// </summary>
        public decimal PriceStaff { get; protected set; }

        /// <summary>
        /// Get the price for guests of the Meal.
        /// </summary>
        public decimal PriceGuests { get; protected set; }

        protected List<Additive> additives = new List<Additive>();

        /// <summary>
        /// Gets a read only list of additives of the meal.
        /// </summary>
        public ReadOnlyCollection<Additive> Additives
        {
            get { return additives.AsReadOnly(); }
        }

        public Meal(MensaPlan plan)
        {
            this.Plan = plan;
        }

        internal abstract void parseData();

        public override string ToString()
        {
            return string.Format("{0}: {1} {2:c}/{3:c}/{4:c} [{5} add.]", Category, Name, PriceStudents, PriceStaff, PriceGuests, additives.Count);
        }
    }
}
