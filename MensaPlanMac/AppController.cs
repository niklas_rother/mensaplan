using System;
using MonoMac.Foundation;
using MonoMac.AppKit;

namespace MensaPlanMac
{
    [Register("AppController")]
    public partial class AppController : NSObject
    {
        public AppController()
        {

        }

        public override void AwakeFromNib()
        {
            var statusItem = NSStatusBar.SystemStatusBar.CreateStatusItem(30);
            statusItem.Menu = statusMenu;
            statusItem.Image = NSImage.ImageNamed("coffee");
            statusItem.HighlightMode = true; //TODO: What does this do?
            statusMenu.RemoveAllItems();

            statusMenu.AddItem(new NSMenuItem("Test", (sender, e) => Console.WriteLine ("click2")));
        }
    }
}

