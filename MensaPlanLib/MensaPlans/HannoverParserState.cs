using System;

namespace MensaPlanLib.MensaPlans
{
	/// <summary>
	/// Represents the state of the mensaplan parser.
    /// The name of the state indicates the type of
    /// text exspected (not read!)
	/// </summary>
    enum HannoverParserState
    {
        Name,
        Date,
        Meals,
        Additives,
    }
}

