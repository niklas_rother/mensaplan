﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MensaPlanLib.Additives
{
    class HannoverAdditive : Additive
    {
        protected static readonly Regex additiveRegex = new Regex(@"^\((?<id>\w\w?)\) (?<name>.*?)( / (?<ename>.*))?$", RegexOptions.ExplicitCapture);

        protected string additiveAsText;

        public HannoverAdditive(string text, MensaPlan plan)
            : base(plan)
        {
            additiveAsText = text;
        }

        internal override void parseData()
        {
            //Example: "(j) artger. Tierhaltung / species-appropr. husbandry"
            var match = additiveRegex.Match(additiveAsText);
            if (!match.Success)
            {
                Name = "Parse Error";
                EnglishName = "Parse Error";
                Identifier = "?";
                return;
            }

            Identifier = match.Groups["id"].Value;
            Name = match.Groups["name"].Value;
            EnglishName = match.Groups["ename"].Value;
        }
    }
}
